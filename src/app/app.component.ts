import { Component, OnInit } from '@angular/core';

declare const $:any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  title = 'app';

  dtOptions: any = {};

  ngOnInit(): void {
    this.dtOptions = {
      bPaginate: false,
      pageLength:10,
      dom: 'Bfrtip',
      colReorder: {
        
      },
      fixedHeader: true,
      columnDefs: [
        { width: "5%", targets: 0 }
      ],
      buttons: [
        'colvis',
        'copy',
        'print',
        'excel',
        'pdf',
        {
          text: 'Some button',
          key: '1',
          action: function (e, dt, node, config) {
            alert('Button activated');
          }
        }
      ],
      columns: [{
        title: 'ID',
        data: 'id'
      }, {
        title: 'First name',
        data: 'firstName'
      }, {
        title: 'Last name',
        data: 'lastName'
      }],
      responsive: true,
      language:{
        "sDecimal":        ",",
        "sEmptyTable":     "Tabloda herhangi bir veri mevcut değil",
        "sInfo":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
        "sInfoEmpty":      "Kayıt yok",
        "sInfoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ".",
        "sLengthMenu":     "Sayfada _MENU_ kayıt göster",
        "sLoadingRecords": "Yükleniyor...",
        "sProcessing":     "İşleniyor...",
        "sSearch":         "Ara:",
        "sZeroRecords":    "Eşleşen kayıt bulunamadı",
        "oPaginate": {
            "sFirst":    "İlk",
            "sLast":     "Son",
            "sNext":     "Sonraki",
            "sPrevious": "Önceki"
        },
        "oAria": {
            "sSortAscending":  ": artan sütun sıralamasını aktifleştir",
            "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
        }
    }
    };
  }

}
